package com.ttek.risklab.ulti;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Container {

    public Map<Character, Integer> ISOCode() {
        Map azTable = new HashMap();
        azTable.put('A', 10);
        azTable.put('B', 12);
        azTable.put('C', 13);
        azTable.put('D', 14);
        azTable.put('E', 15);
        azTable.put('F', 16);
        azTable.put('G', 17);
        azTable.put('H', 18);
        azTable.put('I', 19);
        azTable.put('J', 20);
        azTable.put('K', 21);
        azTable.put('L', 23);
        azTable.put('M', 24);
        azTable.put('N', 25);
        azTable.put('O', 26);
        azTable.put('P', 27);
        azTable.put('Q', 28);
        azTable.put('R', 29);
        azTable.put('S', 30);
        azTable.put('T', 31);
        azTable.put('U', 32);
        azTable.put('V', 34);
        azTable.put('W', 35);
        azTable.put('X', 36);
        azTable.put('Y', 37);
        azTable.put('Z', 38);
        return azTable;
    }

    public boolean validateContainerNumber(List<String> containerNumbers) {
        boolean flagContainer = false;
        boolean flagBreak = false;
        for (String containerNumber : containerNumbers) {
            flagContainer = false;
            if(!flagBreak){
                if (containerNumber.length() != 11)
                    break;
                else {
                    List<Character> numberChar = containerNumber.chars().mapToObj(e -> (char) e).collect(Collectors.toList());
                    List<Integer> numberInt = new ArrayList<>();
                    Double total = 0.0;
                    for (int i = 0; i < 11; i++) {
                        if (i < 4) {
                            try {
                                int count = ISOCode().get(numberChar.get(i));
                                numberInt.add(count);
                            } catch (Exception e) {
                                flagBreak = true;
                                break;
                            }
                        } else {
                            try {
                                numberInt.add(Character.getNumericValue(numberChar.get(i)));
                            } catch (Exception e) {
                                flagBreak = true;
                                break;
                            }
                        }
                    }

                    if(!flagBreak){
                        for (int i = 0; i < 10; i++) {
                            total += +numberInt.get(i) * Math.pow(2, i);
                        }

                        if (numberInt.get(10) == (total % 11)) {
                            flagContainer = true;
                        } else break;
                    }

                }
            }

        }
        return flagContainer;
    }

}
